# **Installation**

Paste to [mineserverFolder]

## List of mods

| № | Name | Is need for client | file name | Link |
| --- | --- | --- | --- | --- |
| 1. | Twilight forest | True | twilightforest-1.18.2-4.1.1494-universal.jar | <https://www.curseforge.com/minecraft/mc-mods/the-twilight-forest> |
| 2. | Oh The Biomes You'll Go | True | Oh_The_Biomes_You'll_Go-forge-1.18.2-1.4.7.jar | <https://www.curseforge.com/minecraft/mc-mods/oh-the-biomes-youll-go> |
| 3. | TerraBlender (Forge) (Core for "Oh The Biomes You'll Go") | True | TerraBlender-forge-1.18.2-1.2.0.126.jar | <https://www.curseforge.com/minecraft/mc-mods/terrablender> |
| 4. | Iron Chests | True | ironchest-1.18.2-13.2.11.jar | <https://www.curseforge.com/minecraft/mc-mods/iron-chests> |
| 5. | Cyclic | True | Cyclic-1.18.2-1.7.14.jar | <https://www.curseforge.com/minecraft/mc-mods/cyclic> |
| 6. | Bookshelf (Core for Cyclic) | True | Bookshelf-Forge-1.18.2-13.2.52.jar | <https://www.curseforge.com/minecraft/mc-mods/bookshelf> |
| 7. | Curios API (Forge) (Core for Cyclic) | True | curios-forge-1.18.2-5.0.9.0.jar | <https://www.curseforge.com/minecraft/mc-mods/curios> |
| 8. | CraftTweaker (Core for Cyclic) | True | CraftTweaker-forge-1.18.2-9.1.205.jar | <https://www.curseforge.com/minecraft/mc-mods/crafttweaker> |
| 9. | JEITweaker (compatibility for jei and crafttweaker) | True | JEITweaker-1.18.2-3.0.0.9.jar | <https://www.curseforge.com/minecraft/mc-mods/jeitweaker> |
| 10. | Patchouli (Core for Cyclic) | True | Patchouli-1.18.2-71.1.jar | <https://www.curseforge.com/minecraft/mc-mods/patchouli> |
| 11. | Clumps | False | Clumps-forge-1.18.2-8.0.0+17.jar | <https://www.curseforge.com/minecraft/mc-mods/clumps> |
| 12. | Create | True | create-1.18.2-0.5.0.i.jar | <https://www.curseforge.com/minecraft/mc-mods/create> |
| 13. | Macaw's Bridges | True | mcw-bridges-2.0.7-mc1.18.2forge.jar | <https://www.curseforge.com/minecraft/mc-mods/macaws-bridges> |
| 14. | Carry On | True | carryon-1.18.2-1.17.0.8.jar | <https://www.curseforge.com/minecraft/mc-mods/carry-on> |
| 15. | Flywheel (Required for Create) | True | flywheel-forge-1.18.2-0.6.8.a.jar | <https://www.curseforge.com/minecraft/mc-mods/flywheel> |
| 16. | Ice and Fire (Deleted, Not compatibility with Multiverse-Core) | True | iceandfire-2.1.12-1.18.2-beta.jar | <https://www.curseforge.com/minecraft/mc-mods/ice-and-fire-dragons>
| 17. | Citadel Mod (Core for Ice and Fire) | True | citadel-1.11.3-1.18.2.jar | <https://www.curseforge.com/minecraft/mc-mods/citadel>
| 18. | SimpleBackups | False | SimpleBackups-1.18.2-1.1.12.jar | <https://www.curseforge.com/minecraft/mc-mods/simple-backups>
| 19. | Industrial Craft Reborn | True | indreb-1.18.2-0.13.0 | <https://www.curseforge.com/minecraft/mc-mods/industrial-reborn> |
| 20. | Forbbiden and Arcanus | True | forbidden_arcanus-1.18.2-2.1.2 | <https://www.curseforge.com/minecraft/mc-mods/forbidden-arcanus/files/4457443> |
| 21. | Valhelsia Core ( Core for Forbbiden and Arcanus ) | True | valhelsia_core-forge-1.18.2-0.4.0 | <https://www.curseforge.com/minecraft/mc-mods/valhelsia-core> |
| 22. | Valhelsia Structures | True | valhelsia_structures-forge-1.18.2-0.1.0 | <https://www.curseforge.com/minecraft/mc-mods/valhelsia-structures>
| 23. | Botania | True | Botania-1.18.2-435 | <https://www.curseforge.com/minecraft/mc-mods/botania> |
| 24. | Mowzie's Mobs | True | mowziesmobs-1.5.32 | <https://www.curseforge.com/minecraft/mc-mods/mowzies-mobs> |
| 25. | Geckolib Core ( Core For Mowzie Mobs ) | True | geckolib-forge-1.18-3.0.57 | <https://www.curseforge.com/minecraft/mc-mods/geckolib> |
| 26. | Waystones | True | waystones-forge-1.18.2-10.2.0 | <https://www.curseforge.com/minecraft/mc-mods/waystones> |
| 27. | Balm Core | True | balm-3.2.6 | <https://www.curseforge.com/minecraft/mc-mods/balm> |
| 28. | When Dungeons Arise | True | DungeonsArise-1.18.2-2.1.52-release | <https://www.curseforge.com/minecraft/mc-mods/when-dungeons-arise> |
| 29. | Alex mobs | True | alexsmobs-1.18.6 | <https://www.curseforge.com/minecraft/mc-mods/alexs-mobs> |
| 29. | chunkpregenerator | False | Chunk+Pregenerator-1.18-4.2.2.jar | <https://www.curseforge.com/minecraft/mc-mods/chunkpregenerator> |
| 30. | Falling Tree | True | FallingTree-1.18.2-3.5.5 | <https://www.curseforge.com/minecraft/mc-mods/falling-tree> |

## List of plugins

| № | Name | Link |
| --- | --- | --- |
| 1. | EssentialsX | <https://www.curseforge.com/minecraft/bukkit-plugins/essentialsx> |
| 2. | Multiverse-Core | <https://www.curseforge.com/minecraft/bukkit-plugins/multiverse-core> |
| 3. | Multiverse-Portals | <https://www.curseforge.com/minecraft/bukkit-plugins/multiverse-portals> |
| 4. | Multiverse-NetherPortals | <https://www.curseforge.com/minecraft/bukkit-plugins/multiverse-netherportals> |
| 5. | WorldEdit for Bukkit | <https://www.curseforge.com/minecraft/bukkit-plugins/worldedit> |
| 6. | SilkSpawners | <https://www.curseforge.com/minecraft/bukkit-plugins/silkspawners> |
| 7. | Mc2Discord | <https://www.curseforge.com/minecraft/mc-mods/mc2discord> |
